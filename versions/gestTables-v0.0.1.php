<?php
/*!******************************************************************
fichier: gestLignes.php
auteur : Pascal TOLEDO
date :28 avril 2015
date de modification: 28 avril 2015
tutos: http://www.legral.fr/intersites/lib/legral/php/gestPDO
git:https://git.framasoft.org/legraLibs/gestPDO
depend de:
	* gesLib.php
	* configDB.php
description:
	* surcouche pdo
tutoriel:
	* php et mysql en utf-8: http://electron-libre.fassnet.net/utf8.php
	* http://php.net/manual/fr/book.pdo.php
***********************************************************************/
$gestLib->loadLib('gestTables',__FILE__,'0.0.1','gestionnaire de Tables et de lignes');
$gestLib->libs['gestTables']->git='https://git.framasoft.org/legraLibs/gestPDO';
/***********************************************************************


/********************************************
 * class gestLigne
********************************************/
class gestLigne{
	public $tableNom='';
	public $indexNom='id'; // nom de l'index pour selectionner la ligne dans la table
	public $indexVal;	// valeur de l'index correspondant à la ligne à séléctionnnée
	public $legralPDO=NULL;
	public $attrs;
	public $erreurs;

	function __toString(){
		return gestLib_inspect(__class__,$this);
		}

	// idVal: id a chercher dans la table
	// idNom: nom du champ servant de id
	// lire la ligne ayant idNom=idVal
	function __construct(&$legralPDO,$tableNom,$_indexVal,$_indexNom='id'){
		global $gestLib;
		$this->legralPDO=$legralPDO;
		$this->tableNom=$tableNom;
		$this->indexVal=$_indexVal;
		$this->indexNom=$_indexNom;
		$this->attrs=new gestAttributs();	
		$this->erreurs=new gestTextes();
		
		//echo $gestLib->debugShowVar('gestLignes',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,__CLASS__.'$this->lignes',$this->legralPDO->isConnect());
	}

	function __destruct(){
		//global $gestLib;echo $gestLib->debugShow('gestTables',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'destruction de '.__CLASS__);
		unset ($this->legralPDO);
		unset ($this->attrs);
		unset ($this->erreurs);
	}

	function load(){
		//global $gestLib;
		
		$this->legralPDO->sql->clear();
		$this->legralPDO->sql->setFROM($this->tableNom);
		$this->legralPDO->sql->setOPERATION("SELECT *");
		$this->legralPDO->sql->setWHERE($this->legralPDO->sql->strChamp($this->indexNom,$this->indexVal));
		$this->legralPDO->query();
		while ($_ligne=$this->legralPDO->fetch()){foreach ($_ligne as $key => $val){$this->attrs->set($key,$val);}}
		$this->legralPDO->queryClose();
	}


	// - renvoie dans un array le nom des champs  - //
	function getChampsNoms(){
		//$first_value = reset($array); // First Element's Value
		//$first_key = key($array); // First Element's Key

		$t=$this->attrs->get();
		$champsNoms=array();
		reset($this->attrs);
		$indexValFirst=key($this->attrs);
		//echo gestLib_inspect('$indexValFirst',$indexValFirst,LEGRALERR::DEBUG,__LINE__,__FUNCTION__,__CLASS__);
		foreach($t as $_k => $_v){$champsNoms[]=$_k;}

		return $champsNoms;
		}


	function get($key=NULL){return $this->attrs->get($key);}

	function set($key,$val=NULL){
		global $gestLib;
		//echo $gestLib->debugShowVar('gestTables',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$key',$key);
		//echo $gestLib->debugShowVar('gestTables',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$val',$val);
		if(is_array($key)){
			foreach($key as $_k => $_v){
				$this->attrs->set($_k,$_v);
			}
		}
		else{
			$this->attrs->set($key,$val);
		}
	}

	// - manipulation du sql - //
	function update($_datas=NULL){
		global $gestLib;
		echo $gestLib->debugShow('gestTables',LEGRALERR::DEBUG,NULL,__CLASS__.'->'.__FUNCTION__.'():'.__LINE__,"");
		if(is_array($_datas))$this->set($_datas);	// maj des datas si défini ET en array

		$this->legralPDO->sql->clear();
		$op='UPDATE `'.$this->tableNom.'` SET ';
		$champs='';
		foreach ($this->attrs->get() as $key => $val){
			if(empty($key))continue;
			//echo $gestLib->debugShow('gestTables',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,__CLASS__."key: $key, val:$val");
			if($champs!=='')$champs.=', ';
			$champs.=$this->legralPDO->sql->strChamp($key,$val);
		}
		$op.=$champs;
		$this->legralPDO->sql->setOPERATION($op);
		$this->legralPDO->sql->setWHERE($this->legralPDO->sql->strChamp($this->indexNom,$this->indexVal));
		$this->legralPDO->query();
		$this->legralPDO->queryClose();
	}



	// - affichage - //
	function tableau(){
		global $gestLib;
		$t=$this->attrs->get();
		$o='';
		$o.='<table>';
		$o.='<caption>'.$this->tableNom.'('.$this->indexNom.':'.$this->indexVal.')</caption>';
		$o.=$this->tableauTHEAD();

		$o.='<tbody>';
		$o.=$this->tableauTR();
		$o.='</tbody>';
		
		$o.='</table>'."\n";
		return $o;
	}

	function tableauTHEAD(){
		$t=$this->attrs->get();
		$o='<thead>';
		$o.='<th>';foreach($t as $_k => $_v){$o.="<td>$_k</td>";}	$o.='</th>';
		$o.='</thead>'."\n";
		return $o;
	}

	function tableauTR(){
		global $gestLib;
		$t=$this->attrs->get();
		$o='';

		$o.='<tr>';
		$o.='<td style="width:10px;"></td>'."\n";// ajoute une colonne virtuelle pour compenser celle qui est aouter auto (WTF??!)
		foreach($t as $_v)	{$o.="<td>$_v</td>";}
		$o.='</tr>'."\n";
		return $o;
	}
} //class gestLigne

/********************************************
 * class gestTable
********************************************/
class gestTable{
	public $dbSelect;
	public $tableNom='';
	public $indexNom='id'; // nom de la colonne servant a indentifier l'index de la table
	public $dbTable;// connexion db pour les manips sur la table
	public $dbLignes;// connexion db commune aux manips sur les lignes
	public $erreurs;
	//indexNom: nom du champ servant de id
	function __construct($dbSelect,$tableNom,$indexNom='id'){
		global $gestLib;
		$this->dbSelect=$dbSelect;
		$this->dbTable=new legralPDO($dbSelect);
		$this->dbLignes=new legralPDO($dbSelect);
		$this->tableNom=$tableNom;
		$this->indexNom=$indexNom;

		$this->erreurs=new gestTextes();
		$this->lignes=array();
	}

	function __destruct(){
		global $gestLib;echo $gestLib->debugShow('gestTables',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'destruction de '.__CLASS__);

	}

	function isConnect(){return $this->dbTable->dbConnect->isConnect();}

	// - load() - //
	// - charge toutes les lignes d'une table - //
	// $indexNom: nom de l'index qui sert à indexer les lignes
	function load(){
		global $gestLib;
		//$this->dbTable->sql->clear();	// garder l'heritage de la personalisation de l'user (donc NE PAS activer)
		$this->dbTable->sql->setFROM($this->tableNom);

		// -  si rien de defini on charge tout ! - //
		if($this->dbTable->sql->getOPERATION()==NULL) $this->dbTable->sql->setOPERATION("SELECT *");

		$this->dbTable->query();

		unset ($this->lignes);
		$this->lignes=array();
		
		//echo $gestLib->debugShowVar('gestTables',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$this->indexNom',$this->indexNom);
		//echo $gestLib->debugShowVar('gestTables',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$this->dbTable->sql->getSQL()',$this->dbTable->sql->getSQL());
		echo '<div class="gestLib_inspectOrigine">';
		while ($_ligne=$this->dbTable->fetch()){
			$ligneId=$_ligne[$this->indexNom];
			$ligneId=$_ligne['joursId'];
			//echo gestLib_inspect('$ligneId',$ligneId,LEGRALERR::DEBUG,__LINE__,__FUNCTION__,__CLASS__);
			//echo gestLib_inspect('$_ligne',$_ligne,LEGRALERR::DEBUG,__LINE__,__FUNCTION__,__CLASS__);
			$this->set($ligneId,$_ligne);
		}
		echo '</div>';
		$this->dbTable->queryClose();
	}



	// - renvoie dans un array le nom des champs  - //
	function getChampsNoms(){
		//$first_value = reset($array); // First Element's Value
		//$first_key = key($array); // First Element's Key
		reset($this->lignes);
		$indexValFirst=key($this->lignes);
		//echo gestLib_inspect('$indexValFirst',$indexValFirst,LEGRALERR::DEBUG,__LINE__,__FUNCTION__,__CLASS__);
		return $this->lignes[$indexValFirst]->getChampsNoms();	// attention SI indexVal nul
	}

	function get($ligneId=NULL,$key=NULL){

		// - si pas de pligne précisé -> construction d'un tableau avec toutes les données - //
		if($ligneId===NULL){
			// - construit un tableau avec toutes les données - //
			$lignesDatas=array();// 
			foreach($this->lignes as $ligneId => $_ligne){
				$lignesDatas[$ligneId]=$_ligne->get();
			}
			return $lignesDatas;
		}

		return (isset($this->lignes[$ligneId]))
			?$this->lignes[$ligneId]->get($key)	// on renvoie la ligne ou 1 champ de la ligne sous forme de tableau
			//:NULL;
			:'pas de ligne';
	}
	// - creait/modifie la ligne ou un champ de la ligne - //
	// retour:
	// 1: modif ok
	// 2: la ligne n'existait pas ->creation
	function set($ligneId,$key,$val=NULL){
		global $gestLib;
		$r=1;
//		echo $gestLib->debugShowVar('gestTables',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$this->tableNom',$this->tableNom);
//		echo $gestLib->debugShowVar('gestTables',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$ligneId',$ligneId);
//		echo $gestLib->debugShowVar('gestTables',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$this->indexNom',$this->indexNom);
//		echo $gestLib->debugShowVar('gestTables',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$key',$key);
//		echo $gestLib->debugShowVar('gestTables',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$val',$val);

		//						function __construct    (&$legralPDO,     $tableNom,      $idVal,        $idNom='id')
		if(!isset($this->lignes[$ligneId])){
			//		function __construct (&$legralPDO,   $tableNom,      $idVal,  $idNom='id'){
			//		$ligne1=new gestLigne($db,           "sandBox",      2,       "joursId");
			$this->lignes[$ligneId]=new gestLigne($this->dbLignes,$this->tableNom,$ligneId,$this->indexNom);
			$r=2;// la ligne n'existait pas ->creation
		}
		$this->lignes[$ligneId]->set($key,$val);	// on renvoie la ligne ou 1 champs de la ligne sous forme de tableau
		//echo gestLib_inspectOrigine('$this->lignes['.$ligneId.']->get()',$this->lignes[$ligneId]->get(),__LINE__,__FUNCTION__);
		//$table->lignes[1]->get()
		return $r;
	}

	// - Manipulation de la db - //

	// -- DELETE d'une ligne -- //
	//  
	public function deleteLigne($ligneId){
		global $gestLib;
		reset($this->lignes);
		if(isset($this->lignes[$ligneId])){
			// - suppr de ma ligne mémoire- //
			unset($this->lignes[$ligneId]);
			// - suppr de la ligne dans la db - //
			$this->dbTable->sql->setFROM($this->tableNom);
			$this->dbTable->sql->setOPERATION('DELETE  ');
			$this->dbTable->sql->setWHERE($this->dbTable->sql->strChamp($this->indexNom,$ligneId));

			echo $gestLib->debugShowVar('gestTables',LEGRALERR::DEBUG,NULL,__CLASS__.':'.__FUNCTION__.':'.__LINE__,'$this->dbTable->sql->getSQL()',$this->dbTable->sql->getSQL());
			$this->dbTable->query();
			$this->dbTable->queryClose();
		}
	}
	
	
	// -- UPDATE ou INSERT d'une ligne -- //
	//  $ligneData: array contenant les datas de la ligne
	public function updateLigne($ligneId,$ligneData=NULL){
		global $gestLib;
		if($this->set($ligneId,$ligneData)===2){		// maj (ou creation) de la ligne en memoire
			$this->insertLigne($ligneId,$ligneData);	// la ligne n'existait pas ->creation de la ligne dans la db (avec les données en memoire)
		}
		else{	// la ligne existe
//echo $gestLib->debugShowVar('gestTables',LEGRALERR::DEBUG,NULL,__CLASS__.':'.__FUNCTION__.':'.__LINE__,'$this->get($ligneId)',		$this->get($ligneId));
echo $gestLib->debugShowVar('gestTables',LEGRALERR::DEBUG,NULL,__CLASS__.':'.__FUNCTION__.':'.__LINE__,'$this->lignes[$ligneId]->get()',$this->lignes[$ligneId]->get());

			if(is_array($ligneData=NULL)){
				echo $gestLib->debugShow('gestTables',LEGRALERR::DEBUG,NULL,__CLASS__.':'.__FUNCTION__.':'.__LINE__,'la ligne existe:$ligneData:array()');
				$this->lignes[$ligneId]->update($ligneData);
			}

			if($ligneData===NULL){
				echo $gestLib->debugShow('gestTables',LEGRALERR::DEBUG,NULL,__CLASS__.':'.__FUNCTION__.':'.__LINE__,'la ligne existe:$ligneData:NOTarray():$this->lignes[$ligneId]->update()');
				$this->lignes[$ligneId]->update();	// version qui devrait tetre normal
				//$this->lignes[$ligneId]->update($this->get($ligneId));	// version pour "compensé" le bug de la maj sans donnée!
			}
		}
	}

	// -- INSERT d'une ligne -- //
	// (fonction interne. Utiliser updateLigne pour inssérer)
	private function insertLigne($ligneId,$_ligneData){
		global $gestLib;
		//echo $gestLib->debugShow('gestTables',LEGRALERR::DEBUG,__LINE__,__CLASS__.':'.__FUNCTION__,' insertLigne ');

		$this->dbTable->sql->clear();

		$names='';	// liste formaté des noms
		foreach($this->getChampsNoms() as $_champNom){
			if($names!=='')$names.=',';
			$names.="`$_champNom`";
		}


		$values='';
		reset($_ligneData);
		foreach($_ligneData as $_val){
			if($values!=='')$values.=',';
			if(!is_numeric($_val)){$values.="'$_val'";}
			else{$values.=$_val;}
		}

		$this->dbTable->sql->setOPERATION('INSERT INTO `'.$this->tableNom."`($names) VALUES ($values);  ");
		echo $gestLib->debugShowVar('gestTables',LEGRALERR::DEBUG,__LINE__,__CLASS__.':'.__FUNCTION__,'$this->dbTable->sql->getSQL()',$this->dbTable->sql->getSQL());
		$this->dbTable->query();
		$this->dbTable->queryClose();
		// - creation de la ligne memoire - //
		$this->set($ligneId,$_ligneData);
	}




	// --  UPDATE de la db entiere -- //
	function update(){
		global $gestLib;
		reset($this->lignes);
		foreach($this->lignes as $_ligneId =>$_ligne){
			//echo $gestLib->debugShowVarOrigine('gestTables',LEGRALERR::DEBUG,__LINE__,__CLASS__.':'.__FUNCTION__,'$_ligneId',$_ligneId);
			$this->updateLigne($_ligneId);
		}
	}


	// - affichage - //
	function tableau(){
		global $gestLib;

		$o='<table>';
		$o.='<caption>'.$this->tableNom.'('.$this->indexNom.')</caption>';
		reset($this->lignes);
		$o.=$this->lignes[key($this->lignes)]->tableauTHEAD();

		$o.='<tbody>';

		reset($this->lignes);
		foreach($this->lignes as $_ligneId => $val){
			$o.=$ligneId=$this->lignes[$_ligneId]->tableauTR();//	echo gestLib_inspect('$key',$key,LEGRALERR::DEBUG,__LINE__,__FUNCTION__,__CLASS__);
		}
		$o.='</tbody>';
		
		$o.='</table>'."\n";
		return $o;
	}


} //class gestTable



$gestLib->setEtat('gestTables',LEGRAL_LIBETAT::LOADED);
$gestLib->end('gestTables');
?>
