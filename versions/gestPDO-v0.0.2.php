<?php
/*!******************************************************************
fichier: gestPDO.php
auteur : Pascal TOLEDO
date :01 fevrier 2012
date de modification: 19 avril 2015
source: http://www.legral.fr/intersites/lib/perso/php/gestPDO
depend de:
	* gesLib.php
	* configDB.php
description:
	* surcouche pdo
tutoriel:
	* php et mysql en utf-8: http://electron-libre.fassnet.net/utf8.php
	* http://php.net/manual/fr/book.pdo.php
***********************************************************************/
$gestLib->loadLib('gestPDO',__FILE__,'0.0.1','gestionnaire de connexion et requetage SQL');
$gestLib->libs['gestPDO']->git='https://git.framasoft.org/legraLibs/gestPDO';
/***********************************************************************

/********************************************
FONCTION GLOBAL
********************************************/
/*
function isNULLwrite($is){
	if ($is==NULL){return 'DEFAULT';}
	return (is_numeric($is)?$is:'\''.addslashes($is).'\'');
}
*/

/********************************************
objet de donnee de db
********************************************/
class legralDatabaseConnect{
	public $host=NULL;
	public $port=NULL;
	public $login=NULL;
	public $pwd=NULL;
	public $databaseName=NULL;
	public $pdo=NULL;
	private $isConnect=0; // 1: connexion au serveur reussi
//	public $erreurs;

	function __toString(){
		return gestLib_inspect(__class__,$this);
		}
 
	function __construct($db){
		global $gestLib;
		$this->erreurs=new gestTextes();
 		$this->databaseName=$db;
		$_config=gestPDO_selectDB($db);
		//echo $gestLib->libs['gestPDO']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,'gestPDO_selectDB($db)',$_config,'nobr');
		if(is_array(gestPDO_selectDB($db))){
			$this->host=isset($_config['host'])?$_config['host']:'localhost';
			$this->port=isset($_config['port'])?$_config['port']:3306;
			$this->login=isset($_config['login'])?$_config['login']:NULL;
			$this->pwd=isset($_config['pwd'])?$_config['pwd']:NULL;
			$this->databaseName=isset($_config['database'])?$_config['database']:NULL;

			if ($r=$this->connect_mySQL()===1){
				$this->pdo->query("SET NAMES 'utf8'");
			}
			else{
				echo $gestLib->libs['gestPDO']->debugShow(LEGRALERR::DEBUG,__LINE__,__METHOD__,'Connexion au serveur sql impossible');

			}
			//echo $gestLib->libs['gestPDO']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,'r',$r);
		}
		else{//aucune db concordante
			//echo $gestLib->libs['gestPDO']->debugShow(LEGRALERR::DEBUG,__LINE__,__METHOD__,'Aucune db concordante: '.$db);
			$gestLib->erreurs['gestPDO']->setNo('NoDBSelectable');


		}
	//return $this;
	}

	function __destruct(){/*echo 'legralDatabaseConnect __destruct()';/*$this->clear();*/}

	function isConnect(){return $this->isConnect;}
	
	function clear(){
		$this->host=NULL;
		$this->port=NULL;
		$this->login=NULL;
		$this->pwd=NULL;
		$this->databaseName=NULL;
		$this->pdo=NULL;
	}
	
	function connect_mySQL(){
		global $gestLib;

		$Connection=($this->host[0]==='/')
			?'unix_socket='.$this->host
			:'host='.$this->host.';port='.$this->port.';';

//		echo $gestLib->libs['gestPDO']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,' exeption e=',);
		try{
			$pdo_arg="mysql:".'dbname='.$this->databaseName.';'.$Connection;
			$this->pdo = new PDO($pdo_arg,$this->login,$this->pwd);
		}
		catch (PDOException $e){

//			echo $gestLib->libs['gestPDO']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,' exeption e=',$e);
			$eTxt=$e->getMessage();
			//echo $gestLib->libs['gestPDO']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,' exeption getMessage=',$eTxt);

			$gestLib->erreurs['gestPDO']->setNo('e');
			$gestLib->erreurs['gestPDO']->setTexte('e',$eTxt);
//echo $gestLib->libs['gestPDO']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,'$gestLib->erreurs["gestPDO"]->textes',$gestLib->erreurs["gestPDO"]->textes);

			//return $e->getMessage();
			$this->isConnect=-1;// - indique une exeption - //
			return 0;
		}

		//echo $gestLib->libs['gestPDO']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,' pdo',$this->pdo);
		$this->isConnect=1;
		return 1;
		//return $this->pdo;
	}
} //class legralDatabaseConnect


/********************************************
objet de gestion d'une chaine sql
********************************************/
class legralSQLData{
	public $operation=NULL;//INSERT xxx SELECT xxx UPDATE xxx etc
	public $where=NULL;
	public $from=NULL;
	public $inner=NULL;	// deprecated
	public $join=NULL;
	public $orderby=NULL;
	public $limit=NULL;
//	public $erreur=0;
//	public $warning=0;

//	function __toString(){	}

	function __construct($from=NULL)
		{
		if (!$from){return(NULL);}
		setFrom($from);
		}
	function __destruct()		{$this->clear();}
	function isLegralSQLData(){return(1);}

	function clear()
		{
		$this->operation=NULL;
		$this->where=NULL;
		$this->from=NULL;
		$this->inner=NULL;
		$this->join=NULL;
		$this->orderby=NULL;
		$this->limit=NULL;
		}
	function getFROM()     {return $this->from;}	 function setFROM($tables){$this->from=$tables;}
	function getINNER()    {return $this->inner;}	 function setINNER($inner){$this->inner=$inner;}
	function getJOIN()    {return $this->join;}	 function setJOIN($join){$this->join=$join;}
	function getOPERATION(){return $this->operation;}function setOPERATION($op){$this->operation=$op;}
	function getWHERE()    {return $this->where;}	 function setWHERE($where){$this->where=$where;}
	function getORDERBY()   {return $this->orderby;} function setORDERBY($orderby){$this->orderby=$orderby;}
	function getLIMIT()    {return $this->limit;}	 function setLIMIT($limit){if(is_numeric($limit))$this->limit=$limit;}

	// - renvoie un champ formater selon val est numeric - //
	// - utilisable pour un UPDATE ou un WHERE - //
	// - noQuote:ne met pas les quote meme si texte - //
	function formatChamp($val,$noQuote=0){
		if($val===NULL) return NULL;
		if(is_numeric($val) OR $noQuote===1 ) return$val;
		return "'$val'";
	}

	// - renvoie une chaine avec key=val ou key="val" selon val est numeric - //
	// - utilisable pour un UPDATE ou un WHERE - //
	function strChamp($key,$val,$signe='=',$noQuote=0){
		if(empty($key))return '';
		$o="`$key`$signe";
		if($val===NULL) return "$o NULL ";
		if(is_numeric($val) OR $noQuote===1)return "$o$val ";
		return "$o'$val'";
	}

	function getSQL()
		{
		$out=$this->operation."\n";
		if (!empty($this->from))    $out.=' FROM `'.$this->from."`";
		if (!empty($this->inner))   $out.=' '.$this->inner;
		if (!empty($this->join))    $out.=' '.$this->join;
		if (!empty($this->where))  $out.=' WHERE '.$this->where;
		if (!empty($this->orderby)) $out.=' ORDER BY '.$this->orderby;
		if (!empty($this->limit))   $out.=' LIMIT '.$this->limit;
		return $out;
		}

	// --- Gestion du WHERE---
	// chaine des AND venant d'un tableau($conditions)
	//cf repereTemporel-0.6.php function rt_PDO2JSON(&$_legralPDO,$tag,$varJS)

/*
fonction addXXX($champ,$valeur)

Attention(ANNULE): Dans le cas de texte pour l'arg $valeur le texte doit etre entre guillement 
	ex: addAND('fete',"'Sonia'")
	sauf dans le cas de tableau, dans ce cas chaque valeur-text sera entouré automatiquement de guillemets simples 
*/

	// si valeur absent alors on ajout le champ en tant que valeur ADD $champ
	// si valeur present 
	function addAND($champ,$valeur=null)
		{global $gestLib;
		if (empty($champ)){return NULL;}

		if (($valeur!=null)AND(!is_numeric($valeur))){$valeur="'$valeur'";}//on l'apostrophe si pas numeric
		// => '','xxx',1
		$val=($valeur==null)?$champ:"$champ=$valeur";	// 
//		if ($valeur==null)
//			{echo $gestLib->libs['gestPDO']->debugShow(LEGRALERR::DEBUG,__LINE__,__METHOD__,__CLASS__.' $val:vide','br');}
		echo $gestLib->libs['gestPDO']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,__CLASS__.' $val',$val,'br');
		if (empty($this->where)){$this->where=$val;return;}
		if ($valeur==null){$val="($val)";}
		if ( strpos($this->where,'OR')!==FALSE )
			{$this->where='('.$this->where.")AND $val";}	// il y a au moins 1 OR
		else
			{$this->where.=" AND $val";}				//il n'y a que des AND
		}		
	function addOR($champ,$valeurs=null)
		{
		if (empty($champ)){return NULL;}
		$or=$this->giveOR($champ,$valeurs);
		if ($or!='')
			{
			if (!$this->where){$this->where=$or;return;}
			if ( strpos($this->where,'AND')!==FALSE )
				{$this->where='('.$this->where.")OR ($or)";}	// il y a au moins 1 AND
			else	{$this->where.=" OR $or";}				//il n'y a que des OR
			}
		}
		
	function giveOR($champ,$valeurs=null)
		{global $gestLib;
		if (empty($champ)){return NULL;}
		$out='';
		echo $gestLib->libs['gestPDO']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,__CLASS__.' $valeurs',$valeurs,'br');
		if (is_array($valeurs))
			{
			$or='';
			foreach ($valeurs as $valeur)
				{
				if (!is_numeric($valeur)){$valeur="'$valeur'";}//on l'apostrophe si pas numeric	// ICI ON LE FAIT

				if ($or!=''){$or.=' OR ';}
				$or.=" $champ=$valeur";
				}		
			$out="($or)";
			}
		else
			{
			if (($valeurs!=null)AND(!is_numeric($valeurs))){$valeurs="'$valeurs'";}//on l'apostrophe si pas numeric
			// => '','xxx',1
			$val=($valeurs==null)?$champ:"$champ=$valeurs";	// 
			if ($valeurs==null)
				{echo $gestLib->libs['gestPDO']->debugShow(LEGRALERR::DEBUG,__LINE__,__METHOD__,__CLASS__.' $val:vide','br','br');}
			echo $gestLib->libs['gestPDO']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,__CLASS__.' $val',$val,'br');
			if (empty($this->where)){$this->where=$val;return;}
			if ($valeurs==null){$val="($val)";}
			$out=$val;
			}
		return $out;
		}
} //class legralSQLData


/********************************************
objet de gestion 
********************************************/
class legralPDO{
	public $dbSelect;
	public $dbConnect=NULL;	//objet (->pdo: ptr sur pdo)
	
	public $sql;
	public $queryReponse=NULL;
	public $lignes;
	//public $erreur=0;
//	public $warning=0;

	function __toString(){
		return gestLib_inspectOrigine('',$this);
	}

	function __construct($db){
//		if (empty($db)){return NULL;}
		$this->dbSelect=$db;
		$this->dbConnect=new legralDatabaseConnect($db);
		$this->sql=new legralSQLData();
		}

	function __destruct()
		{
		$this->queryClose();
		$this->dbConnect=NULL;
		$this->sql=NULL;
		$this->queryReponse=NULL;
		$this->lignes=NULL;
		}

	function isConnect(){return $this->dbConnect->isConnect();}


	// - http://www.php.net/manual/fr/pdo.getattribute.php - //
	function getAttribut($attribut){
		$out='';
		if (empty($attribut)){
			$out.=' PDO::ATTR_AUTOCOMMIT: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_AUTOCOMMIT")).'<br />';
			$out.='| PDO::ATTR_ERRMODE: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_ERRMODE")).'<br />';
			$out.='| PDO::ATTR_CASE: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_CASE")).'<br />';
			$out.='| PDO::ATTR_CLIENT_VERSION: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_CLIENT_VERSION")).'<br />';
			$out.='| PDO::ATTR_CONNECTION_STATUS: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_CONNECTION_STATUS")).'<br />';
			$out.='| PDO::ATTR_PERSISTENT: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_PERSISTENT")).'<br />';
			//$out.='| PDO::ATTR_PREFETCH: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_PREFETCH")).'<br />';
			$out.='| PDO::ATTR_SERVER_INFO: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_SERVER_INFO")).'<br />';
			$out.='| PDO::ATTR_SERVER_VERSION: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_SERVER_VERSION")).'<br />';
			//$out.='| PDO::ATTR_TIMEOUT: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_TIMEOUT")).'br />';
		}
		else	$out.='| PDO::ATTR_'.$attribut.': '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_$attribut"));
		return $out;
	}
	

	function str_erreur(){
		switch ($erreur)
			{
			case 0: return '';break;
			case 1: return ('connexion &agrave; la '.$this->database.' impossible');break;
			}
	}


	// - dialogue avec la db - //
	// -- envoie du sql -- //
	// return $this->sql->getSQL()
	// $sql: envoie directement le contennue a query sinon $this->sql->getSQL()
	function query($sql=NULL,$fetch=NULL)
		{global $gestLib;
		if (!empty($this->dbConnect->pdo)){
			$_sql=($sql!==NULL)?$sql:$this->sql->getSQL();

			echo $gestLib->debugShowVar('gestPDO',LEGRALERR::DEBUG,'',__CLASS__.':'.__METHOD__.':'.__LINE__,'$_sql',$_sql,'br');

			$this->queryReponse=$this->dbConnect->pdo->query($this->sql->getSQL()) ;
			//echo $gestLib->debugShowVarOrigine('gestPDO',LEGRALERR::DEBUG,'',__CLASS__.':'.__METHOD__.':'.__LINE__,' $this->queryReponse',$this->queryReponse,'br');
			if ($fetch!==NULL){	$this->queryReponse->setFetchMode($fetch);}

			//echo $gestLib->libs['gestPDO']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__FUNCTION__,__CLASS__.' $this->queryReponse',$this->queryReponse,'br'); // ne pas activer vide le flux!!!
			}
		else{
			echo $gestLib->libs['gestPDO']->debugShow(LEGRALERR::DEBUG,__LINE__,__METHOD__,__CLASS__,'non connecté au serveur sql' );
		}
	return 	$_sql;
	}

	// -- reinitialisation du dialogue(fermeture de la connexion ?) -- //
	function queryClose($clear=1){
		if (!empty($this->queryReponse)){
			if ($clear){$this->sql->clear();}
			$this->lignes=NULL;
			$this->queryReponse->closeCursor();
		};
	}

	// - manipulation du resultat du query 
	// http://www.phpro.org/tutorials/Introduction-to-PHP-PDO.html#8.7

	// -- mise en array de l'enregistrement pointé par le curseur de queryReponse -- //
	// -- PDOStatement::fetch — Récupère la ligne suivante d'un jeu de résultats PDO  -- //
	// http://php.net/manual/fr/pdostatement.fetch.php
	function fetch($order=PDO::FETCH_NAMED){
		global $gestLib;
		if (!empty($this->queryReponse)){
			$this->lignes=empty($this->queryReponse)?false:$this->queryReponse->fetch($order);
			//echo $gestLib->debugShowVarOrigine('gestPDO',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,__CLASS__.' $this->lignes',$this->lignes,'br');
			return($this->lignes);
		}
		else {
			//echo $gestLib->debugShowOrigine('legralPDO',LEGRALERR::DEBUG,__LINE__,__METHOD__,'pas de réponse sql');
			}

	}



	// -- mise en objet de l'enregistrement pointé par le curseur de queryReponse -- //
	// http://php.net/manual/fr/pdostatement.fetchobject.php
	function fetch_class($classeNom='stdClass'){
		global $gestLib;
		if (!empty($this->queryReponse)){
			echo $gestLib->libs['gestPDO']->debugShow(LEGRALERR::DEBUG,__LINE__,__FUNCTION__,__CLASS__.' fetchMode: fetchObject','br');
			return $this->queryReponse->fetchObject($classeNom);
			}
		else {
			echo $gestLib->libs['gestPDO']->debugShow(LEGRALERR::DEBUG,__LINE__,__METHOD__,'pas de réponse sql');
			}
	}


	// - lignes2JSON - //
	// - Utiliser apres fetch ? - //
	function lignes2JSON(){
		if($this->lignes){
			$nb=0;$out='{';
			foreach($this->lignes as $key => $val){
				if ($value!=''){
					if($nb++){$out.=',';}//ajouter une virgule avant l'ajout sauf pour le 1er ajout
					if (!is_numeric($val)){$val="'$val'";}
					$out.="$key:$val";
				}
			}
			$out.='}';
			return $out;
		}
		else{return NULL;}
	}

	// - tableau2JSON - //
	// - transforme un array en JSON - //
	function tableau2JSON($tableau)
		{
		if($tableau)
			{
			$nb=0;$out='{';
			foreach($tableau as $key => $value)
				{
				if($nb++){$out.=',';}
				if (!is_numeric($value)){$value="'$value'";}
				$out.="$key:$value";
				}
			$out.='}';
			return $out;
			}
		else{return NULL;}
		}
} //class legralPDO

$gestLib->setEtat('gestPDO',LEGRAL_LIBETAT::LOADED);
$gestLib->end('gestPDO');
?>
