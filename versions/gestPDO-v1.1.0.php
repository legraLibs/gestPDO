<?php
/*!******************************************************************
fichier: legralpdo-1.1.php
version : 1.1
auteur : Pascal TOLEDO
date :01 fevrier 2012
source: http://www.legral.fr/intersites/lib/perso/php/legralpdo
depend de:
	* gesLib-0.1.php
	* /www/www-pwd/mysqlPHP/legral_databases.php
description:
	* surcouche pdo
tutoriel:
	* php et mysql en utf-8: http://electron-libre.fassnet.net/utf8.php
	* http://php.net/manual/fr/book.pdo.php
***********************************************************************/
$gestLib->loadLib('legralpdo',__FILE__,'1.1','gestionnaire de connexion et requetage SQL');

/***********************************************************************
// APPEL DES DONNEES DE CONNEXIONS
***********************************************************************/
require_once(DOCUMENT_ROOT.'mysqlPHP/legral_databases_pwd-1.0.php');

//------------------------------------------------------------------
//  Creates a new mySQL database connection
// source : http://www.php.net/manual/fr/ref.pdo-mysql.connection.php
function dbPDO_Connect_mySQL($DBUser,$DBPass,$DBName=FALSE,$DBHost=FALSE,$DBPort=FALSE)
{	$DBNameEq=empty($DBName)?'':";dbname=$DBName";
	if (empty($DBHost)) $DBHost = 'localhost';
	if ($DBHost[0]==='/'){$Connection="unix_socket=$DBHost";}else{if(empty($DBPort))$DBPort=3306;$Connection="host=$DBHost;port=$DBPort";}
	try	{$dbh = new PDO("mysql:$Connection$DBNameEq", $DBUser, $DBPass);}
	catch (PDOException $e){return $e->getMessage();}
	return $dbh;
}

/********************************************
FONCTION GLOBAL
********************************************/
function isNULLwrite($is)
	{
	if ($is==NULL){return 'DEFAULT';}
	return (is_numeric($is)?$is:'\''.addslashes($is).'\'');
	}

/*
//================================
//================================
//
//Example of use:
//connects to the default (localhost)
$dbh = dbPDO_Connect_mySQL('user', 'password', 'database');
//error handler goes here
if (!is_object($dbh)) trigger_error("Failed to connect to 'database'  | Error = $dbh", E_USER_ERROR);
//get a record
$sql = "select * from SomeTable limit 1;";
$dbRS = $dbh->query($sql);
$row = empty($dbRS) ? false : $dbRS->fetch(PDO::FETCH_ASSOC);
if (!empty($dbRS))	$dbRS->closeCursor();
//do something with the data
if (!empty($row))	print_r($row);   
else	echo "Error? no data found\n";
*/
/********************************************
objet de donnee de db
********************************************/
class legralDatabaseConnect
{
	public $host=NULL;
	public $port=NULL;
	public $user=NULL;
	public $pwd=NULL;
	public $databaseName=NULL;
	public $pdo=NULL;

	function __toString()
		{
		$out='<b>'. __CLASS__.'</b><ul>';
		$out.="<li>host: $this->host</li>";
		$out.="<li>port: $this->port</li>";
		$out.="<li>user: $this->user</li>";
		$out.="<li>pwd: *****</li>";
		$out.="<li>databaseName: $this->databaseName</li>";
		return $out.'</ul>';
		}

	function __construct($db=NULL)
		{global $gestLib;
		if (empty($db)){return NULL;}
		$this->databaseName=$db;
		if ( legral_db_prepare($this) )
			{
			if (!$this->connect_mySQL())
				{
				$erreur=1;
				
				return NULL;
				}
			else	{$this->pdo->query("SET NAMES 'utf8'");}
			}
		}
	function __destruct()		{$this->clear();		}
	function isLegralDatabase(){return 1;}
	function clear()
		{
		$this->host=NULL;
		$this->port=NULL;
		$this->user=NULL;
		$this->pwd=NULL;
		$this->databaseName=NULL;
		$this->pdo=NULL;
		}
	function isDataOk()
		{
		if (!$this->host){$this->host='localhost';}
		if (!$this->port){$this->port='3306';}
		if (!$this->user){return NULL;}
		if (!$this->pwd){return NULL;}
		if (!$this->databaseName){return NULL;}
		return 1;
		}
		
	function connect_mySQL()
		{global $gestLib;
		if (!$this->isDataOk())	{return NULL;}
		else	{
			if ($this->host[0]==='/')
				{$Connection='unix_socket='.$this->host;}
			else	{$Connection='host='.$this->host.';port='.$this->port.';';}
			}

		try	{
			$pdo_arg="mysql:".'dbname='.$this->databaseName.';'.$Connection;
			echo $gestLib->lib['legralpdo']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,' $this->user',$this->user,'br');
			//echo $gestLib->lib['legralpdo']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,' $this->pwd',$this->pwd);
			$this->pdo = new PDO($pdo_arg,$this->user,$this->pwd);
			}
		catch (PDOException $e)
			{
			echo $gestLib->lib['legralpdo']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,' exeption',$e->getMessage(),'br');
			//return $e->getMessage();
			}
		echo $gestLib->lib['legralpdo']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,' pdo',$this->pdo,'br');

		return $this->pdo;
		}
}

/********************************************
objet de gestion d'une chaine sql
********************************************/
class legralSQLData
{
	public $operation=NULL;//INSERT xxx SELECT xxx UPDATE xxx etc
	public $where=NULL;
	public $from=NULL;
	public $inner=NULL;
	public $orderby=NULL;
	public $erreur=0;
	public $warning=0;

	function __toString()
		{
		$out='<b>'. __CLASS__.'</b><ul>';
		foreach ($this as $key => $value){$out.="<li>$key: $value</li>";}
		return $out.'</ul>';
		}

	function __construct($from=NULL)
		{
		if (!$from){return(NULL);}
		setFrom($from);
		}
	function __destruct()		{$this->clear();}
	function isLegralSQLData(){return(1);}

	function clear()
		{
		$this->operation=NULL;
		$this->where=NULL;
		$this->from=NULL;
		$this->inner=NULL;
		$this->orderby=NULL;
		}
	function setFROM($tables){$this->from=$tables;}
	function setINNER($inner){$this->inner=$inner;}
	function setOPERATION($op){$this->operation=$op;}
	function setORDERBY($orderby){$this->orderby=$orderby;}
	
	function getSQL()
		{
		$out=$this->operation."\n";
		if ($this->from!==NULL){$out.=' FROM '.$this->from."\n";}
		if ($this->inner!==NULL) {$out.=$this->inner."\n";}
		if ($this->where!==NULL) {$out.=' WHERE '.$this->where."\n";}
		if ($this->orderby!==NULL) {$out.=' ORDER BY '.$this->orderby."\n";}
		return $out;
		}

	// --- Gestion du WHERE---
	// chaine des AND venant d'un tableau($conditions)
	//cf repereTemporel-0.6.php function rt_PDO2JSON(&$_legralPDO,$tag,$varJS)

/*
fonction addXXX($champ,$valeur)

Attention(ANNULE): Dasn le cas de texte pour l'arg $valeur le texte doit etre entre guillement 
	ex: addAND('fete',"'Sonia'")
	sauf dasn le cas de tableau, dans ce cas chaque valeur-text sera entouré automatiquement de guillemets simples 
*/

	// si valeur absent alors on ajout le champ en tant que valeur ADD $champ
	// si valeur present 
	function addAND($champ,$valeur=null)
		{global $gestLib;
		if (empty($champ)){return NULL;}

		if (($valeur!=null)AND(!is_numeric($valeur))){$valeur="'$valeur'";}//on l'apostrophe si pas numeric
		// => '','xxx',1
		$val=($valeur==null)?$champ:"$champ=$valeur";	// 
//		if ($valeur==null)
//			{echo $gestLib->lib['legralpdo']->debugShow(LEGRALERR::DEBUG,__LINE__,__METHOD__,__CLASS__.' $val:vide','br');}
		echo $gestLib->lib['legralpdo']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,__CLASS__.' $val',$val,'br');
		if (empty($this->where)){$this->where=$val;return;}
		if ($valeur==null){$val="($val)";}
		if ( strpos($this->where,'OR')!==FALSE )
			{$this->where='('.$this->where.")AND $val";}	// il y a au moins 1 OR
		else
			{$this->where.=" AND $val";}				//il n'y a que des AND
		}		
	function addOR($champ,$valeurs=null)
		{
		if (empty($champ)){return NULL;}
		$or=$this->giveOR($champ,$valeurs);
		if ($or!='')
			{
			if (!$this->where){$this->where=$or;return;}
			if ( strpos($this->where,'AND')!==FALSE )
				{$this->where='('.$this->where.")OR ($or)";}	// il y a au moins 1 AND
			else	{$this->where.=" OR $or";}				//il n'y a que des OR
			}
		}
		
	function giveOR($champ,$valeurs=null)
		{global $gestLib;
		if (empty($champ)){return NULL;}
		$out='';
		echo $gestLib->lib['legralpdo']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,__CLASS__.' $valeurs',$valeurs,'br');
		if (is_array($valeurs))
			{
			$or='';
			foreach ($valeurs as $valeur)
				{
				if (!is_numeric($valeur)){$valeur="'$valeur'";}//on l'apostrophe si pas numeric	// ICI ON LE FAIT

				if ($or!=''){$or.=' OR ';}
				$or.=" $champ=$valeur";
				}		
			$out="($or)";
			}
		else
			{
			if (($valeurs!=null)AND(!is_numeric($valeurs))){$valeurs="'$valeurs'";}//on l'apostrophe si pas numeric
			// => '','xxx',1
			$val=($valeurs==null)?$champ:"$champ=$valeurs";	// 
			if ($valeurs==null)
				{echo $gestLib->lib['legralpdo']->debugShow(LEGRALERR::DEBUG,__LINE__,__METHOD__,__CLASS__.' $val:vide','br','br');}
			echo $gestLib->lib['legralpdo']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,__CLASS__.' $val',$val,'br');
			if (empty($this->where)){$this->where=$val;return;}
			if ($valeurs==null){$val="($val)";}
			$out=$val;
			}
		return $out;
		}


}
/********************************************
objet de gestion 
********************************************/

class legralPDO
{
	public $db_connect=NULL;	//objet (->pdo: ptr sur pdo)
	public $sql=NULL;
	
	public $queryReponse=NULL;
	public $ligne=NULL;
	public $erreur=0;
	public $warning=0;

	function __toString()
		{
		$out='<b>'. __CLASS__.'</b><ul>';
		$out.="<li>db_connect: $this->db_connect</li>";
		$out.="<li>sql: $this->sql</li>";
		$out.="<li>queryReponse: PDO Object</li>";
		$out.="<li>ligne: $this->ligne</li>";
		$out.="<li>erreur: $this->erreur</li>";
		$out.="<li>warning: $this->warning</li>";
		return $out.'</ul>';
		}

	function __construct($db=NULL)
		{
		if (empty($db)){return NULL;}
		$this->db_connect=new legralDatabaseConnect($db);
		$this->sql=new legralSQLData();
		}

	function __destruct()
		{
		$this->queryClose();
		$this->db_connect=NULL;
		$this->sql=NULL;
		$this->queryReponse=NULL;
		$this->ligne=NULL;
		}
	function isLegralPDO(){return 1;}
	function getAttribut($attribut)
		{//http://www.php.net/manual/fr/pdo.getattribute.php
		$out='';
		if (empty($attribut))
			{
			$out.=' PDO::ATTR_AUTOCOMMIT: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_AUTOCOMMIT")).'<br />';
			$out.='| PDO::ATTR_ERRMODE: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_ERRMODE")).'<br />';
			$out.='| PDO::ATTR_CASE: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_CASE")).'<br />';
			$out.='| PDO::ATTR_CLIENT_VERSION: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_CLIENT_VERSION")).'<br />';
			$out.='| PDO::ATTR_CONNECTION_STATUS: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_CONNECTION_STATUS")).'<br />';
			$out.='| PDO::ATTR_PERSISTENT: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_PERSISTENT")).'<br />';
			//$out.='| PDO::ATTR_PREFETCH: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_PREFETCH")).'<br />';
			$out.='| PDO::ATTR_SERVER_INFO: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_SERVER_INFO")).'<br />';
			$out.='| PDO::ATTR_SERVER_VERSION: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_SERVER_VERSION")).'<br />';
			//$out.='| PDO::ATTR_TIMEOUT: '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_TIMEOUT")).'br />';
			}
		else{
			$out.='| PDO::ATTR_'.$attribut.': '.$this->db_connect->pdo->getAttribute(constant("PDO::ATTR_$attribut"));
			}
		return $out;
		}
	

	function str_erreur()
		{
		switch ($erreur)
			{
			case 0: return '';break;
			case 1: return ('connexion &agrave; la '.$this->database.' impossible');break;
			}
		}

	function query($fetch=NULL)
		{global $gestLib;
		if (!empty($this->sql->operation))
			{
			echo $gestLib->lib['legralpdo']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,__CLASS__.' $this->sql->getSQL()',$this->sql->getSQL(),'br');
			$this->queryReponse=$this->db_connect->pdo->query($this->sql->getSQL()) ;

			if ($fetch!==NULL){	$this->queryReponse->setFetchMode($fetch);}
			echo $gestLib->lib['legralpdo']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__FUNCTION__,__CLASS__.' $this->queryReponse',$this->queryReponse,'br');
			}
		else {
			echo $gestLib->lib['legralpdo']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,__CLASS__.' pas d\'operation fournit',$this->sql->operation,'br');
			}
		}

	//http://www.php.net/manual/fr/pdostatement.fetch.php // voir exemple
	//http://www.phpro.org/tutorials/Introduction-to-PHP-PDO.html#8.7
	function query_FETCH_CLASS(&$instance=NULL,$classeNom=NULL)
		{global $gestLib;
		if (!empty($this->sql->operation))
			{
			echo $gestLib->lib['legralpdo']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,__CLASS__.' $this->sql->getSQL()',$this->sql->getSQL(),'br');
			$this->queryReponse=$this->db_connect->pdo->query($this->sql->getSQL()) ;
			$instance=$this->queryReponse->fetchObject($classeNom);
			echo $gestLib->lib['legralpdo']->debugShow(LEGRALERR::DEBUG,__LINE__,__FUNCTION__,__CLASS__.' fetchMode: fetchObject','br');
			}
		else {
			echo $gestLib->lib['legralpdo']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,__CLASS__.' pas d\'operation fournit',$this->sql->operation,'br');
			}
		}



	function queryClose($clear=1)
		{
		if (!empty($this->queryReponse))
			{
			if ($clear){$this->sql->clear();}
			$this->ligne=NULL;
			$this->queryReponse->closeCursor();
			};
		}
		
	function readLigne()
		{global $gestLib;
		$this->ligne=empty($this->queryReponse)?false:$this->queryReponse->fetch(PDO::FETCH_NAMED);
		//echo $gestLib->lib['legralpdo']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__FUNCTION__,__CLASS__.' $this->ligne',$this->ligne,'br');

		return($this->ligne);
		}
	function ligne2JSON()
		{
		if($this->ligne)
			{
			$nb=0;$out='{';
			foreach($this->ligne as $key => $value)
				{
				if ($value!='')
					{
					if($nb++){$out.=',';}//ajouter une virgule avant l'ajout sauf pour le 1er ajout
					if (!is_numeric($value)){$value="'$value'";}
					$out.="$key:$value";
					}
				}
			$out.='}';
			return($out);
			}
		else{return NULL;}
		}

	function tableau2JSON($tableau)
		{
		if($tableau)
			{
			$nb=0;$out='{';
			foreach($tableau as $key => $value)
				{
				if($nb++){$out.=',';}
				if (!is_numeric($value)){$value="'$value'";}
				$out.="$key:$value";
				}
			$out.='}';
			return $out;
			}
		else{return NULL;}
		}
}
//$gestLib->setEtat('legralpdo',LEGRAL_LIBETAT::LOADED);
$gestLib->end('legralpdo');
?>
