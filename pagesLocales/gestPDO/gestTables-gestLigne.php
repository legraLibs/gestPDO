<?php
global $gestLib;$dbTest;

function randomString($length = 10) {
    return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
}

?>
<h1 class="h1">$gestLigne: gestion d'UNE ligne d'une table</h1>

<h2 class="h2">Creation d'une ligne</h2>
<ol>
	<li>Connexion à la base 'test', table: 'sandBox', Valeur de l'index: 2, nom de l'index: 'joursId'</li>
	<li>creation de la ligne</li>
</ol>
 <div class="noteimportant">La connexion avec la db doit pré-initialisé!'</div>


<?php
$cmd='$db= new legralPDO("test");'."\n";
$cmd.='$ligne1=new gestLigne($db,"sandBox",2,"joursId");'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
echo gestLib_inspect('$ligne1->legralPDO->isConnect()',$ligne1->legralPDO->isConnect());
echo gestLib_inspectOrigine('$ligne1',$ligne1);
echo gestLib_inspectOrigine('$ligne1->get()',$ligne1->get());
echo gestLib_inspectOrigine('$ligne1->tableau()',$ligne1->tableau());

if($ligne1->legralPDO->isConnect()<=0){
	echo '<div class="noteimportant">';
	echo 'Connexion impossible<br>';
	$eTxt=$gestLib->erreurs['gestPDO']->getTexte('e');
	if($eTxt!=''){
		echo "exeption:$eTxt<br>";
	}
	echo '</div>';
	unset ($db);

}
else{
	echo 'Connexion OK<br>';
}
?>

<h2 class="h2">Chargement de la ligne à partir de la db</h2>

<?php
$cmd='$ligne1->load();'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
?>

<h2 class="h2">Lire les noms des champs de la ligne</h2>
<?php
$cmd='$ligne1->getChampsNoms();'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';//eval($cmd);

echo gestLib_inspect('$ligne1->getChampsNoms()',$ligne1->getChampsNoms());
?>


<h2 class="h2">Lire les valeurs des champs de la ligne</h2>
<?php
$cmd ='$ligne1->get();'."\n";
$cmd.='$ligne1->attrs->get();'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';//eval($cmd);

echo gestLib_inspect('$ligne1->attrs->get()',$ligne1->attrs->get());
echo gestLib_inspect('$ligne1->get()',$ligne1->get());
?>


<h2 class="h2">Lire la valeur d'un champ de la ligne</h2>
<?php
$cmd='$ligne1->get("fete");'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';//eval($cmd);

echo gestLib_inspect('$ligne1->get("fete")',$ligne1->get("fete"));
?>


<h2 class="h2">Sauver la ligne dans une variable (array)</h2>
<?php
$cmd='$backup=$ligne1->get();'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
echo gestLib_inspect('$backup',$backup);
?>


<h2 class="h2">Creer/modifier la ligne à partir d'un tableau</h2>
<?php
$cmd='$l=array();$l["joursId"]=700;$l["mois"]=50;$l["jour"]=200;$l["fete"]="la mienne";$l["type"]="evidemment";'."\n";
$cmd.='$ligne1->set($l);'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
echo '<br>'.gestLib_inspect('$ligne1->get()',$ligne1->get());
?>

<h2 class="h2">Restaurer la ligne a partir d'un tableau</h2>
<?php
$cmd='$ligne1->set($backup);'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
echo gestLib_inspect('$backup',$backup);
?>



<h2 class="h2">Creer/modifier un champ</h2>
<?php
$_t=randomString();
$cmd=''."\n";
$cmd='$ligne1->set("fete","'.$_t.'");'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
echo '<br>'.gestLib_inspect('$ligne1->get()',$ligne1->get());
?>

<h2 class="h2">Update de la db</h2>
<ol>
	<li>sauver les modif</li>
	<li>recharger</li>
	<li>afficher</li>
</ol>
<?php
$cmd='$ligne1->update();'."\n";echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
?>
<div class="noteimportant">Tous les champs doivent être fournis, et , dans l'ordre de la lecture de la table!<br>
<b>Vérifier que la mémoire est bien modifié!</b>
</div>

<h3 class="h3">Les variables mémoires sont-elles bien modifiées?</h3>
<?php echo gestLib_inspectOrigine('$ligne1->get()',$ligne1->get());?>

<h3 class="h3">La db est elle bien modifié?</h3>
<?php $cmd='$ligne1->load();'."\n";echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);?>
<div class="gestLib_inspectOrigine"><?php echo $ligne1->tableau(); ?></div>



<h2 class="h2">Soyons courtois redonnons à Basile sa fete! - update db avec données </h2>
<?php
$cmd=''."\n";
//$cmd='$ligne1->set("fete","Basile");'."\n";


$cmd='$datas=array();'."\n";
$cmd.='$datas["fete"]="Basile";'."\n";
$cmd.='$datas["type"]="neutre";'."\n";

$cmd.='$ligne1->update($datas);'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
?>
<h3 class="h3">Les variables mémoires sont-elles bien modifiées?</h3>
<?php echo gestLib_inspectOrigine('$ligne1->get()',$ligne1->get());?>

<h3 class="h3">La db est elle bien modifié?</h3>
<?php $cmd='$ligne1->load();'."\n";echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);?>
<div class="gestLib_inspectOrigine"><?php echo $ligne1->tableau(); ?></div>




<h2 class="h2">INSERT: creation de la ligne dans la db</h2>
cf: gestTables


<h2 class="h2">tableau()</h2>
<?php
$cmd='echo $ligne1->tableau();'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
?>


<h2 class="h2">Fermeture</h2>
<?php
$cmd='unset ($ligne1);'."\n";
//$cmd.='unset ($ligne4);'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
?>


