<?php
global $gestLib;$dbTest;


function randomString($length = 10) {
    return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
}

?>

<h1 class="h1"><a name="gestTable">$gestTable: gestion de toutes les lignes d'une table</a></h1>


<h2 class="h2">Connexion à une table<br>
(La table est automatiquement chargé à la construction)</h2>
<?php
$cmd='$table=new gestTable("test","sandBox","joursId");'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);

//gestLib_inspect('$txt',$txt,__LINE__,__FUNCTION__,"br")
echo gestLib_inspect('$table->dbTable->isConnect()', $table->dbTable->isConnect(), __LINE__,__FUNCTION__);
echo gestLib_inspect('$table->dbLignes->isConnect()',$table->dbLignes->isConnect(),__LINE__,__FUNCTION__);
//echo gestLib_inspectOrigine('$table',$table);

if($table->dbTable->isConnect()<=0){
	echo '<div class="noteimportant">Connexion impossible<br>'.$gestLib->erreurs['gestPDO']->getTexte('e').'</div>';
	unset ($db);
}
else{
	echo 'Connexion OK<br>';
}

echo gestLib_inspectOrigine('$table',$table);
echo gestLib_inspectOrigine('$table->get()',$table->get());
echo gestLib_inspectOrigine('$table->tableau()',$table->tableau());

unset($table);
?>


<h2 class="h2">Connexion à une table (avec conditions)</h2>
<?php
$cmd='$table=new gestTable("test","sandBox","joursId",[ "SELECT" => "`joursId`,`mois`,`jour`,`fete`", "WHERE" => "`joursId` < 6", "ORDERBY" => "`joursId` DESC"]);'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
?>



<h2 class="h2">ReChargement de la table a partir de la db<br>
(La condition where de la construction est toujours actif)</h2>
<div class="gestLib_inspectOrigine">
<?php
$cmd=''."\n";
$cmd.='$table->load();'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
?>
</div>
<h3 class="h3">Lecture des variables mémoires</h3>
<?php echo gestLib_inspectOrigine('$table->get()',$table->get());?>

<h3 class="h3">Afichage en tableau</h3>
<div class="gestLib_inspectOrigine"><?php echo $table->tableau(); ?></div>



<h2 class="h2">Récupérer les noms des champs sous forme d'un array</h2>
<?php
$cmd='$table->getChampsNoms()";'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';//eval($cmd);
echo gestLib_inspectOrigine('$table->getChampsNoms()',$table->getChampsNoms());
?>

<h2 class="h2">Récupérer une ligne sous forme d'un array</h2>
<?php
$cmd='$table->get(5);'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';//eval($cmd);
echo gestLib_inspectOrigine($cmd,$table->get(5));
?>

<h2 class="h2">Récupérer la valeur d'un champ</h2>
<?php
$cmd='$table->get(5,"fete");'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';//eval($cmd);
echo gestLib_inspectOrigine($cmd,$table->get(5,"fete"));
?>



<h2 class="h2">modifier un champ en memoire</h2>
<?php
$cmd=''."\n";
$cmd.='$indexVal=5;'."\n";
$cmd.='$champNom="fete";'."\n";
$cmd.='$champVal="'.randomString().'";'."\n";

$cmd.='$table->set($indexVal,$champNom,$champVal);'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
echo gestLib_inspectOrigine('$table->get($indexVal)',$table->get($indexVal));
?>
<div class="noteimportant">Seuls les variables mémoires sont modifiées. Pour mettre à jours la db faire un $table-&gt;update();</div>
<div class="noteimportant">Si vous ne faite pas un update() (eg:un $table-&gt;update()) vos données seront perdus lors du prochain load();;</div>






<h2 class="h2">INSERT/UPDATE  UNE ligne dans la db: la ligne existe(UPDATE) sans données</h2>
<?php
$_ligneId=5;
$cmd='$table->updateLigne('.$_ligneId.');'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
?>
<h3 class="h3">Les variables mémoires sont elles bien modifiées?</h3>
<?php echo gestLib_inspectOrigine('$table->get('.$_ligneId.')',$table->get($_ligneId));?>

<div class="notewarning">bug: d'ou vient la ligne 6 NULL !!!???!!!</div>

<h3 class="h3">La db est elle bien modifiée?</h3>
<?php $cmd='$table->load();'."\n";echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);?>
<div class="gestLib_inspectOrigine"><?php echo $table->tableau(); ?></div>











<h2 class="h2">INSERT/UPDATE  UNE ligne dans la db: la ligne existe(UPDATE) avec des données</h2>
<?php
$f=randomString();
$_ligneId=3;
$m=rand(0, 255);
$cmd='$_ligneId='.$_ligneId.';'."\n";
$cmd.='$ligneDatas=array();'."\n";
$cmd.='$ligneDatas["mois"]='.$m.';$ligneDatas["fete"]="'.$f.'";$ligneDatas["type"]="autre";'."\n";
$cmd.='$table->updateLigne($_ligneId,$ligneDatas);'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
echo gestLib_inspect('$table->dbTable->sql->getSQL()',$table->dbTable->sql->getSQL());
?>
<h3 class="h3">Les variables mémoires sonr elles bien modifiées?</h3>
<?php echo gestLib_inspectOrigine('$table->get('.$_ligneId.')',$table->get($_ligneId));?>

<h3 class="h3">La db est elle bien modifié?</h3>
<?php $cmd='$table->load();'."\n";echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);?>
<div class="gestLib_inspectOrigine"><?php echo $table->tableau(); ?></div>



<h2 class="h2">INSERT/UPDATE UNE ligne dans la db: la ligne N'existe PAS(INSERT)</h2>
<?php
$f=randomString();
$_ligneId=500;
$m=rand(0, 255);
$j=rand(0, 255);

//$cmd='$_ligneId=500;'."\n";
$cmd='$ligneDatas=array();'."\n";
$cmd.='$ligneDatas["JoursId"]='.$_ligneId.";\n";
$cmd.='$ligneDatas["mois"]='.$m.";\n";
$cmd.='$ligneDatas["jour"]='.$j.";\n";
$cmd.='$ligneDatas["fete"]="C est quand ca?"'.";\n";
$cmd.='$ligneDatas["type"]="neutre";'."\n";
$cmd.='$table->updateLigne('.$_ligneId.',$ligneDatas);'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
?>
<div class="noteimportant">Tous les champs doivent être fournis, et , dans l'ordre de la lecture de la table!<br></div>

<h3 class="h3">Les variables mémoires sont-elles bien modifiées?</h3>
<?php echo gestLib_inspectOrigine('$table->get('.$_ligneId.')',$table->get($_ligneId));?>

<h3 class="h3">La db est elle bien modifié?</h3>
<?php $cmd='$table->load();'."\n";echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);?>
<div class="gestLib_inspectOrigine"><?php echo $table->tableau(); ?></div>



<h2 class="h2">DELETE ligne dans la db</h2>
<?php
$f=randomString();
$_ligneId=500;

$cmd=''."\n";
$cmd.='$table->deleteLigne('.$_ligneId.');'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
?>


<h3 class="h3">Les variables mémoires sonr elles bien modifiées?</h3>
<?php echo gestLib_inspectOrigine('$table->get()',$table->get());?>

<h3 class="h3">La db est elle bien modifié?</h3>
<?php $cmd='$table->load();'."\n";echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);?>
<div class="gestLib_inspectOrigine"><?php echo $table->tableau(); ?></div>



<h2 class="h2">Update global de la db</h2>

<h3 class="h3">On modifie un champ pour l'exemple:</h3>
<?php
$indexVal=2;$champNom='fete';$champVal=randomString();
$cmd='$table->set('.$indexVal.',"'.$champNom.'","'.$champVal.'");'."\n";echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
?>
<h3 class="h3">Update:</h3>
<?php //$cmd='$table->update();'."\n";				echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);?>
<?php $cmd='$table->update(2);'."\n";				echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);?>


<h3 class="h3">Les variables mémoires sont elles bien modifiées?</h3>
<?php echo gestLib_inspectOrigine('$table->get(2)',$table->get(2));?>

<h3 class="h3">La db est elle bien modifié?</h3>
<?php $cmd='$table->load();'."\n";echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);?>
<div class="gestLib_inspectOrigine"><?php echo $table->tableau(); ?></div>






<h2 class="h2">tableau</h2>
<?php
$cmd=''."\n";
$cmd='echo $table->tableau();'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';
?>

<h2 class="h2">tableauTR</h2>
<?php
$cmd=''."\n";
//$cmd='echo $ligne4->tableauTR();'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
?>

<h2 class="h2">tableau()</h2>
<?php
$cmd='echo $table->tableau();'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
?>

