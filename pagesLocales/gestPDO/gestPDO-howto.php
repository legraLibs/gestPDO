<?php global $dbTest;?>
<h1 class="h1">gestPDO - howto</h1>

<h2 class="h2">inclusions</h2>

<span class="coding_filename">./gestPDO.php</span><br>
<pre class="coding_code ">include ('./gestPDO.php');</pre>
Le noyau de la librairie.<br>
<?php //include('./gestPDO.php');?>


<span class="coding_filename">./configDB.php</span><br>
<pre  class="coding_file">
function gestPDO_selectDB($dbSelect){
	$_config=array();
	switch($dbSelect){
	case'test':
		$_config['host']='localhost';
		$_config['port']=3306;
		$_config['login']='anonymous';
		$_config['pwd']='hackme';
		$_config['database']='publique';
		return $_config;
		break;

	case'fail':
		$_config['host']='nowhere';
		$_config['port']=0;
		$_config['login']='';
		$_config['pwd']='';
		$_config['database']='noDB';
		return $_config;
		break;


	}
	return NULL;// pas de concordance
}
</pre>
Contient les informations pour se connecter aux bases de données (y compris le mot de passe en clair).<br>
Ce fichier ne devrait donc pas être dans l'arborescence /www<br>
<pre class="coding_code">include ('./config/configDB.php');</pre>
ou
<pre class="coding_code">include ('./configLocal/configDB.php');</pre>

<?php //include('./configDB.php');?>


<h2 class="h2">Selectionner une connexion</h2>
<?php
//$cmd='$select="fail";'."\n".'$dbTest= new legralPDO($select);';	// for fail test only
$cmd='$select="test";'."\n".'$dbTest= new legralPDO($select);';

echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';//eval($cmd);
echo gestLib_inspectOrigine('$dbTest',$dbTest);

?>

<h2 class="h2">requetage sql - query (création d'un array pour chaque ligne de la réponse du query)</h2>

<?php
$cmd='$dbTest->sql->clear();'."\n"
.'$dbTest->sql->setFROM("ephemeride");'."\n"
.'$dbTest->sql->setWHERE("`type`='.'\''.'masc'.'\''.'");'."\n"
.'$dbTest->sql->setOPERATION("SELECT *");'."\n"
.'$dbTest->sql->setLIMIT(3);'."\n"
.'$dbTest->query();'
;

echo '<pre class="coding_code">'.$cmd.'</pre>';eval($cmd);
echo gestLib_inspect('$dbTest->sql->getSQL()',$dbTest->sql->getSQL());
?>

<h3 class="h3">récupération du resultat brut:$this->queryReponse</h3>
<?php
$cmd='$dbTest->queryReponse';
echo '<pre class="coding_code">'.$cmd.'</pre>';//eval($cmd);
echo gestLib_inspectOrigine('$dbTest->queryReponse',$dbTest->queryReponse);
?>


<h3 class="h3">récupération et manipulation du resultat:fetch</h3>

Lit les enregistrements du resultat ligne par ligne.<br>

<?php
$resultats=array();
$cmd='while ($ligne=$dbTest->fetch()){'."\n";
$cmd.='	$joursId=$ligne["joursId"];'."\n";
$cmd.='	$resultats[$joursId]=array();'."\n";
$cmd.='	foreach ($ligne as $key => $val){'."\n";
$cmd.='		echo gestLib_inspect("key",$key,__LINE__,__METHOD__,"nobr");echo gestLib_inspect("val",$val);'."\n";
$cmd.='		// mise en var'."\n";
$cmd.='		$resultats[$joursId][$key]=$val;'."\n";
$cmd.='	}'."\n";
$cmd.='	echo"<br>";'."\n";
$cmd.='}'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
echo gestLib_inspectOrigine('resultats',$resultats);
?>

<h3 class="h3">réinitilisation du query  et nettoyage des variables internes</h3>
<?php
$cmd='$dbTest->queryClose();'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
?>



<hr />
<h2 class="h2">requetage sql - Formatage de la valeur d'un champ: formatChamp($var,$noQuote=0)</h2>
<?php
echo gestLib_inspect('NULL',NULL);
echo gestLib_inspect('$dbTest->sql->formatChamp(NULL)',$dbTest->sql->formatChamp(NULL));
echo gestLib_inspect('$dbTest->sql->formatChamp("")[empty]',$dbTest->sql->formatChamp(""));
echo gestLib_inspect('$dbTest->sql->formatChamp(22)',$dbTest->sql->formatChamp(22));

echo gestLib_inspect('$noQuote=1;$dbTest->sql->formatChamp("22",$noQuote) (pas d\'ajout de quote)',$dbTest->sql->formatChamp("22",1));
echo gestLib_inspect('$noQuote=1;$emptyIsNULL=1;$dbTest->sql->formatChamp("",$noQuote,$emptyIsNULL))',$dbTest->sql->formatChamp("",0,1));

echo gestLib_inspect('$noQuote=0;$emptyIsNULL=0;$dbTest->sql->formatChamp("22",$noQuote,$emptyIsNULL))',$dbTest->sql->formatChamp("22",0,0));
echo gestLib_inspect('$noQuote=0;$emptyIsNULL=1;$dbTest->sql->formatChamp("",$noQuote,$emptyIsNULL))',$dbTest->sql->formatChamp("",0,1));

echo gestLib_inspect('$dbTest->sql->formatChamp("Texte")(ajout de quote)',$dbTest->sql->formatChamp("Texte"));
?>
<h2 class="h2">requetage sql - Formatage d'un champ et de sa valeur: strChamp($key,$val,$signe='=',$noQuote=0)</h2>
<?php
echo gestLib_inspect('NULL',NULL);
echo gestLib_inspect('$dbTest->sql->strChamp("var",NULL)',$dbTest->sql->strChamp("var",NULL));
echo gestLib_inspect('$dbTest->sql->strChamp("var",22)',$dbTest->sql->strChamp("var",22));
echo gestLib_inspect('$dbTest->sql->strChamp("var","22")',$dbTest->sql->strChamp("var","22"));
echo gestLib_inspect('$dbTest->sql->strChamp("var","sup",">")',$dbTest->sql->strChamp("var","sup",">"));
echo gestLib_inspect('$dbTest->sql->strChamp("var","sansQuote","=",1)',$dbTest->sql->strChamp("var","sansQuote","=",1));
echo gestLib_inspect('$dbTest->sql->strChamp("var","texte")',$dbTest->sql->strChamp("var","texte"));
?>



<hr />
<h2 class="h2">requetage sql - query (création d'un objet(standart) pour chaque ligne de la réponse du query)</h2>

<?php
$cmd='$dbTest->sql->clear();'."\n"
.'$dbTest->sql->setFROM("ephemeride");'."\n"
.'$dbTest->sql->setWHERE("`type`='.'\''.'neutre'.'\''.'");'."\n"
.'$dbTest->sql->setOPERATION("SELECT *");'."\n"
.'$dbTest->sql->setLIMIT(3);'."\n"
.'$dbTest->query();'
;

echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
echo gestLib_inspect('$dbTest->sql->getSQL()',$dbTest->sql->getSQL());
?>
<h3 class="h3">récupération et manipulation du resultat: fetch_class()</h3>
Lit les enregistrements du resultat ligne par ligne.<br>

<?php
$cmd='$ligne=$dbTest->fetch_class();'."\n";

echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
echo gestLib_inspectOrigine('ligne',$ligne);
?>
<h3 class="h3">réinitilisation du query et nettoyage des variables internes</h3>
<?php
$cmd='$dbTest->queryClose();'."\n";
echo '<pre class="coding_code">'.$cmd.'</pre>';eval($cmd);
?>


<hr />
<h2 class="h2">requetage sql - query (création d'un objet pour chaque ligne de la réponse du query)</h2>

<?php
$cmd='$dbTest->sql->clear();'."\n"
.'$dbTest->sql->setFROM("ephemeride");'."\n"
.'$dbTest->sql->setWHERE("`type`='.'\''.'fem'.'\''.'");'."\n"
.'$dbTest->sql->setOPERATION("SELECT *");'."\n"
.'$dbTest->sql->setLIMIT(3);'."\n"
.'$dbTest->query();'
;

echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
echo gestLib_inspect('$dbTest->sql->getSQL()',$dbTest->sql->getSQL());
?>
<h3 class="h3">récupération et manipulation du resultat: fetch_class()</h3>
Lit les enregistrements du resultat ligne par ligne.<br>

<?php
$cmd='';

$cmd.='class enregistrement{'."\n";
$cmd.='    public $joursId;'."\n";
$cmd.='    public $mois;'."\n";
$cmd.='    public $jour;'."\n";
$cmd.='    public $fete;'."\n";
$cmd.='    public $type;'."\n";

$cmd.='    public function __construct(){'."\n";
$cmd.='        $args = func_get_args();'."\n";
$cmd.='        $nargs = func_num_args();'."\n";
$cmd.='        $attribs = get_class_vars(get_class($this));'."\n";
$cmd.='       if(isset($args)){'."\n";
$cmd.='            foreach($args as $value){'."\n";
$cmd.='                $attrib = key($attribs);'."\n";
$cmd.='                $this->$attrib = $value;'."\n";
$cmd.='                next($attribs);'."\n";
$cmd.='            }'."\n";
$cmd.='        }'."\n";
$cmd.='    }'."\n";
$cmd.='}'."\n";


$cmd.='$ligne=$dbTest->fetch_class("enregistrement");'."\n";

echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
echo gestLib_inspectOrigine('ligne',$ligne);
?>
<h3 class="h3">réinitilisation du query et nettoyage des variables internes</h3>
<?php
$cmd='$dbTest->queryClose();'."\n";
echo '<pre class="coding_code">'.$cmd.'</pre>';eval($cmd);
?>



