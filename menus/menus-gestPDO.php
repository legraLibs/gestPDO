<?php
// ==== menu: primaire ==== //
$mn='gestPDO';
$pagePath=PAGESLOCALES_ROOT."$mn/";
$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath.'accueil.html');
	// -- parametrer la page -- //
	$m->setAttr($p,'visible',1);				// 0: le li ne sera pas affiche 1:afficher
	$m->setAttr($p,'menuTitre','lib:gestPDO');		// afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
	$m->setAttr($p,'menuTitle','lib:gestPDO');		// afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
	$m->setAttr($p,'titre','librairie gestPDO');		// titre de la page: afficher dans le bas de page
//        $m->setMeta($p,'title','tutoriels - accueil(meta)');	// meta <title> (si non definit title=titre)
//	$m->addCssA($p,'dossier1');                          // applique le style dossier1 a la balise <a>


$p=$mn.'-howto';
$m->addCallPage($p,$pagePath.$p.'.php');
	$m->setAttr("$p",'menuTitre','howto');

$p=$mn.'-gestErreurs';
$m->addCallPage($p,$pagePath.'gestErreurs.php');
	$m->setAttr("$p",'menuTitre','gestion des erreurs');
	$m->setAttr("$p",'titre',"gestPDO - gestion des erreurs");

$p='gestTables-gestTable';
$m->addCallPage($p,$pagePath.$p.'.php');
	$m->setAttr("$p",'menuTitre','gestion d\'une table');
	$m->setAttr("$p",'titre',"gestPDO - gestion d\'une table");

$p='gestTables-gestLigne';
$m->addCallPage($p,$pagePath.$p.'.php');
	$m->setAttr("$p",'menuTitre','gestion d\'une ligne');
	$m->setAttr("$p",'titre',"gestPDO - gestion d\'une ligne");

$p='typo';// cette page etant aussi un menu, ce menu sera appelle
$m->addCallPage($p,$pagePath.'typo/accueil.html');
	$m->setAttr("$p",'menuTitre','typo');
	$m->setAttr("$p",'titre','typo');
	$m->addCssA("$p",'dossierIconeLeft');

?>
